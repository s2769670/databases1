import java.sql.*;
import java.util.ArrayList;

public class Databases {
    public static Connection connection;
    private static String password = "smth";
    private static String username = "smth";
    static ArrayList<Integer> mids = new ArrayList<>();

    public static void main(String[] args) throws SQLException {
        int iters = 5;
        long startTime = System.currentTimeMillis();
        connection(username, password);
        for (int i=0; i<iters; i++) {
            for (Integer mid : allMovies()) {
                if (!hasDirector(mid)) {
                    ArrayList<String> aut = authorOfMovie(mid);
                }
            }
        }
        long stopTime = System.currentTimeMillis();
        double elapsedTime = (stopTime - startTime) / (1.0*iters);
        System.out.println("Measured time: "+elapsedTime+" ms");
        connection.close();

    }


    public static void connection(String username, String password) throws SQLException {
        try {
            Class.forName("org.postgresql.Driver");
        } catch (ClassNotFoundException cnfe) {
            System.err.println("Error loading driver: " + cnfe);
        }
        String host = "bronto.ewi.utwente.nl";
        String dbName = "dxxx";
        String url = "jdbc:postgresql://"
                + host + ":5432/" + dbName;
        try {
            connection = DriverManager.getConnection(url, username, password);
        } catch (SQLException sqle) {
            System.err.println("Error connecting: " + sqle);
        }
//
//        String query = "SELECT exercise7(?);"
//        PreparedStatement statement = connection.prepareStatement(query);
//        statement.setString(1,"Harrison Ford");
//        ResultSet resultSet = statement.executeQuery();
        //
//        Statement statement = connection.createStatement();
//        String query = "SELECT DISTINCT p1.name\n" +
//                "FROM movies.person p1\n" +
//                "WHERE EXISTS(\n" +
//                "SELECT p2.name\n" +
//                "FROM movies.person p2, movies.acts a, movies.movie m, movies.writes w\n" +
//                "WHERE p2.name = 'Harrison Ford' AND p2.pid = a.pid AND a.mid = m.mid AND p1.pid = w.pid AND w.mid = m.mid\n" +
//                ")"
//        ResultSet resultSet = statement.executeQuery(query);
//        while(resultSet.next()) {
//            System.out.println(resultSet.getString(1);
//            }
        //
//        String query = "SELECT DISTINCT p1.name\n" +
//                "FROM movies.person p1\n" +
//                "WHERE EXISTS(\n" +
//                "SELECT p2.name\n" +
//                "FROM movies.person p2, movies.acts a, movies.movie m, movies.writes w\n" +
//                "WHERE p2.name = ? AND p2.pid = a.pid AND a.mid = m.mid AND p1.pid = w.pid AND w.mid = m.mid\n" +
//                ")";
//        PreparedStatement statement = connection.prepareStatement(query);
//        statement.setString(1,"Harrison Ford");
//        ResultSet resultSet = statement.executeQuery();
        //
//        int iters = 5;
//        long startTime = System.currentTimeMillis();
//        String query="SELECT p1.name\n" +
//                "FROM movies.person p1, movies.movie m, movies.writes w\n" +
//                "WHERE w.mid = m.mid AND w.pid = p1.pid AND NOT EXISTS(\n" +
//                "SELECT DISTINCT p2.name\n" +
//                "FROM movies.person p2, movies.directs d\n" +
//                "WHERE d.mid=m.mid AND d.pid=p2.pid\n" +
//                ")";
//        Statement statement = connection.createStatement();
//        for (int i=0; i<iters; i++) {
//            ResultSet resultSet = statement.executeQuery(query);
//        }
//        for(int i = 0; i<100;i++){
//            statement.close();
//        }
//        long stopTime = System.currentTimeMillis();
//        double elapsedTime = (stopTime - startTime) / (1.0*iters);
//        System.out.println("Measured time: "+elapsedTime+" ms");
//        connection.close();
    }

    private static ArrayList<Integer> allMovies() throws SQLException {     // mids of all movies
        String query = "SELECT DISTINCT m.mid\n" +
                "FROM movies.movie m";
        PreparedStatement statement = connection.prepareStatement(query);
        ResultSet resultSet = statement.executeQuery();
        while (resultSet.next())
            mids.add(resultSet.getInt("mid"));
        statement.close();
        return mids;

    }


    private static ArrayList<String> authorOfMovie(int mid) throws SQLException {  // authors of movie ‘mid’
        ArrayList<String> authorsOfMid = new ArrayList<>();
        String query = "SELECT DISTINCT p.name, m.mid\n" +
                "FROM movies.movie m, movies.person p, movies.writes w\n" +
                "WHERE w.pid = p.pid";
        PreparedStatement statement = connection.prepareStatement(query);
        ResultSet resultSet = statement.executeQuery();
        while (resultSet.next()) {
                if (resultSet.getInt("mid") == mid){
                    authorsOfMid.add(resultSet.getString("name"));
                }
        }
        statement.close();
        return authorsOfMid;
    }

    private static boolean hasDirector(int mid) throws SQLException {       // movie ‘mid’ has a director
        String query = "SELECT DISTINCT m.mid\n" +
                "FROM movies.movie m, movies.person p, movies.directs d\n"+
                "WHERE d.pid = p.pid AND d.mid = m.mid";
        PreparedStatement statement = connection.prepareStatement(query);
        ResultSet resultSet = statement.executeQuery();
        while (resultSet.next()) {
            if (resultSet.getInt("mid") == mid){
                statement.close();
                return true;
            }
        }
        statement.close();
        return false;
    }
}
